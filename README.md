# Setup
## install dependencies
1. Create virtual envirnment
    ```bash
    python -m venv env
    ```

2. Acess the virtual envirnment 
    - see the official documentation https://docs.python.org/3/library/venv.html

3. Install dependencies
    ```bash
    make setup 
    ```

## setup .env
1. create `.env` file
2. Add eden-ai api key to it
    ```
    EDENAI_API_KEY="your_eden_ai_api_key" 
    ```

# Scripts
## 1. Preprocessing

```bash
make preprocessing
```
## 2. Send api calls

```bash
make call_api
```
## Launch Benchmark
```bash
make run_bench
```

# Resuls

# Get the Dataset
1. clone the repo
    ```bash
    git clone https://gitlab.com/abdelhakaissat/ner_benchmark.git
    ```
2. pull the dataset
    ```bash
    dvc pull
    ```
3. follow the links and the steps 
4. wait until it downloads
5. YEY, now you have the `dataset` directory

# dataset directory strucutre
**If it's between `` then it's a comment**
```
dataset
├── file
├── normal
│   ├── Few-NERD(SUP)_datst :``Directory``
│   │   └── ``content of the dataset ``
│   ├── conll2003
│   │   └── ``content of the dataset ``
│   ├── conll2003.zip ``zip format of the above dataset``
│   ├── fewNerds-supervised.zip ``zip format of fewNerds``
│   └── tagger `` random model that i forgot to delete :)``
│       ├── ``all the files that this models needs``
├── responses
│   ├── ibm_responses
│   │   ├── reponse_i.json ``i responses resulting of using ibm's api, by sending dev_transformed elements``
│   ├── reponse_i.json ``i responses resulting of using microsoft,google,amazone apis, by sending dev_transformed elements``
│   ├── random_text
│   │   ├── complete_entity ``directory``
│   │   │   └── reponse_i.json ``responses resulting of using complete entity as input to the api``
│   │   └── incomplete_entity ``directory``
│   │       └── reponse_i.json ``responses resulting of using 1 token entity as input to the api``
└── transformed ``Files that have been transformed!``
    ├── amazone
    │   ├── amazon_parsedi.json ``i parsed  json files``
    │   └── random_text
    │       ├── complete_entity ``directory``
    │       │   └── amazon_parsedi.json ``parsed amazon responses for complete entity request``
    │       └── incomplete_entity ``directory`
    │           └── amazon_parsedi.json `parsed amazon responses for 1 token entity request``
    ├── data_chunk.json ``just a file that i forgot to rmove``
    ├── google ``parsed google files``
    │   ├── google_parsedi.json ``i parsed  json files (normal input)``
    │   └── random_text
    │       ├── complete_entity ``directory``
    │       │   └── google_parsedi.json ``parsed google responses for complete entity request``
    │       └── incomplete_entity ``directory`
    │           └── google_parsedi.json `parsed google responses for 1 token entity request``
    ├── ibm
    │   ├── ibm_parsedi.json ``i parsed  json files``
    │   └── random_text
    │       ├── complete_entity ``directory``
    │       │   └── ibm_parsedi.json ``parsed ibm responses for complete entity request``
    │       └── incomplete_entity ``directory`
    │           └── ibm_parsedi.json `parsed ibm responses for 1 token entity request``
    ├── microsoft
    │   ├── microsoft_parsedi.json ``i parsed  json files``
    │   └── random_text
    │       ├── complete_entity ``directory``
    │       └── incomplete_entity ``directory`
    ├── nerd_transformed ``Transformed nerd dataset to follow spacy 2's format``
    │   ├── dev_transformed.json
    │   ├── test_transformed.json
    │   └── train_transformed.json
    ├── random_text ``Transformed nerd dataset but generated with random text in order to see how providers manage it``
    │   ├── complete_entity ``all the tokens of the entity``
    │   │   ├── dev_transformed_random_all_tok.json
    │   │   ├── test_transformed_random_all_tok.json
    │   │   └── train_transformed_random_all_tok.json
    │   └── incomplete_entity ``1 token only randomly generated text dataset (dont take into account the whole entity just 1 element``
    └── test_transform.json ``random files that i forgot to remove``
```

# Tag naming convention

- starting with **`d`** related to a data transformation, in that tag the datacreated and the script are all there so you can re generate it
- starting with **`v`** means version, not neessary related to data

**!BUT!** this convention doesn't wok for the first 2 tags `v0.0.0` and `v0.0.1`