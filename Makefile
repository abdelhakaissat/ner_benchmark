preprocessing:
	python benchmark_ner/preprocessing.py
setup: requirements.txt
	pip install -r requirements.txt
call_api:
	python benchmark_ner/ner_calls.py
run_bench:
	python benchmark_ner/main.py