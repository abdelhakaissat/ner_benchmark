import pandas as pd
import numpy as np
import json


    

def launch_benchmark(num_parc, dataset, provider_path , provider):
    """launch benchmark

    Parameters
    ----------
    num_parc : int
        first num_parc indexes to "benchmark" 
        TODO:write better explanation
    dataset : dict
        dataset with spacy format 
    provider_path : str
        path to provider's predictions
    provider : str
        provider's name  

    Returns
    -------
    dict 
        structured as this (but with actual numbers not 0):
            {
                "location":{"tp":0,"fp":0,"fn":0}, 
                "organization":{"tp":0,"fp":0,"fn":0},
                "person":{"tp":0,"fp":0,"fn":0},
                "event":{"tp":0,"fp":0,"fn":0}
            }
    """
    tp,tn,fn = 0, 0, 0    

    classes = {"location":{"tp":0,"fp":0,"fn":0}, 
                "organization":{"tp":0,"fp":0,"fn":0},
                 "person":{"tp":0,"fp":0,"fn":0},
                  "event":{"tp":0,"fp":0,"fn":0}
                }

    for i in range(num_parc):
        data_path = f"{provider_path}{provider}_parsed{i}.json"
        predicted_entities_dic = read_data(data_path=data_path)
        
        original_entities_list = dataset[i][1]["entities"]
        predicted_entities_list = predicted_entities_dic["entities"]

        del predicted_entities_dic

        for entity_label in classes.keys():
            original_entities_class = []
            predicted_entities_class = []

            #TODO: ajouter tout les element de la class `entity_label`

            #1) ajouter tt les elements dispo dans origianl
            for original_entity_elem in original_entities_list:
                if original_entity_elem[2] == entity_label:
                    original_entities_class.append(original_entity_elem)
            #2) ajouter tt les elements dispo dans predicted 
            for predicted_entity_elem in predicted_entities_list:
                if predicted_entity_elem[2] == entity_label:
                    predicted_entities_class.append(predicted_entity_elem)
            #3) rajouter les start end en commun (qui sont dans original_entities_class 
            # mais pas dans predicted_entities_class)
            for original_entity_celem in original_entities_class:
                #verifier si  il existe dans p_e_c (start,end)
                # on doit donc parcourir p_e_c
                exists = False
                for predicted_entity_celem in predicted_entities_class:
                    if (    original_entity_celem[0] == predicted_entity_celem[0]
                            and original_entity_celem[1] == predicted_entity_celem[1]):
                        exists = True
                        break
                if not exists:
                    # verifier si il existe dans predicted_tities_list
                    exists = False
                    for predicted_entity_elem in predicted_entities_list:
                        # si il existe dans la liste original mais pas dans celle de la classe il faut l'ajouter  
                        if (    original_entity_celem[0] == predicted_entity_elem[0]
                                and original_entity_celem[1] == predicted_entity_elem[1]):
                            exists = True
                            predicted_entities_class.append(predicted_entity_elem)
                            break
            #4) rajouter les start end en commun (qui sont dans predicted_entities_class 
            # mais pas dans original_entities_class) (le contraire de ce qu'on a fait en haut)
            for original_entity_celem in predicted_entities_class:
                #verifier si  il existe dans p_e_c (start,end)
                # on doit donc parcourir p_e_c
                exists = False
                for predicted_entity_celem in original_entities_class:
                    if (    original_entity_celem[0] == predicted_entity_celem[0]
                            and original_entity_celem[1] == predicted_entity_celem[1]):
                        exists = True
                        break
                if not exists:
                    # verifier si il existe dans predicted_tities_list
                    exists = False
                    for predicted_entity_elem in original_entities_list:
                        # si il existe dans la liste original mais pas dans celle de la classe il faut l'ajouter  
                        if (    original_entity_celem[0] == predicted_entity_elem[0]
                                and original_entity_celem[1] == predicted_entity_elem[1]):
                            exists = True
                            original_entities_class.append(predicted_entity_elem)
                            break
            #5) la on a les 2 lists c'est cool
            # on doit maintenant faire le fameux traitement
            # pour prendre les tp tn et les autres :)

            n_original_entities_class=[]
            n_predicted_entities_class=[]
            #5.1) prendre que les endroit ou start, end sont en commun les autres on s'en fou!!
            for j in original_entities_class:
                for k in predicted_entities_class:
                    if j[0]==k[0] and j[1]==k[1]:
                        n_original_entities_class.append(j)
                        n_predicted_entities_class.append(k)
                        if j[2]==k[2]:
                            classes[entity_label]["tp"] += 1
                        if k[2]==entity_label and j[2]!=entity_label:
                            classes[entity_label]["fp"] += 1
                            # normalement it's correct 
                            if j[2] in classes.keys():
                                classes[j[2]]["fn"] +=1


    # calculate the interesting stuff!
    for i in classes.keys() :
        try:
            classes[i]["precision"] = classes[i]["tp"]/(classes[i]["tp"] + classes[i]["fp"])

            classes[i]["recall"] = classes[i]["tp"]/(classes[i]["tp"] + classes[i]["fn"])
            classes[i]["f1"] = (2 * classes[i]["precision"] * classes[i]["recall"]) / (classes[i]["precision"] + classes[i]["recall"])
        except ZeroDivisionError:
            if (provider == "ibm" and i=="event"):

                print("\nWARNING: IBM provider has a problem with the 'event' class!\n")

        


    print(classes)

    return classes

def read_data(data_path):
    with open(data_path) as file:
        res = json.load(file)
    return res

def get_num_entities(dataset, max_index=0):
    """Get the number of entities in the max_index elements of a dataset

    Parameters
    ----------
    dataset : dict
        following the spacy 2 format for data

    max_index : int, optional
        number of "lines" of the dataset we will use to compute
        the number of entities there is , by default 0
    """
    #TODO: parcourir dataset 
    # only consider the 4 location, organization, person, event
    classes = {"location":0, "organization":0, "person":0, "event":0}
    if max_index==0:
        max_index = len(dataset)

    for i in range(max_index):
        entities_list = dataset[i][1]["entities"]
        for entity in entities_list:
            if entity[2] in classes.keys():
                classes[entity[2]] = classes[entity[2]] + 1
    print("----------------------------number en entitites!----------------------------")
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")
    print(classes)
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")

def get_num_entities_recognized(path_response, provider):
    """get the number of entities recognized

    Parameters
    ----------
    path_response : str
        path to the parsed dataset
    provider : str 
        provider's name
    """
    classes = {"location":0, "organization":0, "person":0, "event":0}
    for i in range(100):
        data_path = f"{path_response}{provider}_parsed{i}.json"
        entities_dic = read_data(data_path=data_path)
        entities_list = entities_dic["entities"]
        for entity in entities_list:
            if entity[2] in classes.keys():
                classes[entity[2]] = classes[entity[2]] + 1
            
    print(f"--------------Number en entitites recognized by {provider}---------------------")
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")
    print(classes)
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------")
        #calculate the number of entites of some sort in a list!

def main():
    preproc_path = "dataset/transformed/random_text/complete_entity/dev_transformed_random_all_tok.json"
    original_data = read_data(data_path=preproc_path)
    get_num_entities(dataset=original_data,max_index= 100)
    
    
    #  amazone  and the other providers prediction paths
    predicted_path_amz = "dataset/transformed/amazone/random_text/complete_entity/"
    predicted_path_ms = "dataset/transformed/microsoft/random_text/complete_entity/"
    predicted_path_ibm = "dataset/transformed/ibm/random_text/complete_entity/"
    predicted_path_ggl = "dataset/transformed/google/random_text/complete_entity/"


    get_num_entities_recognized(path_response=predicted_path_amz, provider="amazon")
    get_num_entities_recognized(path_response=predicted_path_ms, provider="microsoft")
    get_num_entities_recognized(path_response=predicted_path_ibm, provider="ibm")
    get_num_entities_recognized(path_response=predicted_path_ggl, provider="google")



    launch_benchmark(num_parc=100, dataset=original_data, provider_path=predicted_path_ms, provider="microsoft")
        
if __name__ == '__main__':
    # define all the paths
    dataset_path= "dataset/"
    output_path = "results/"
    providers_list = ["microsoft","ibm","google","amazon"]
    metrics_list = [""]

    main()
