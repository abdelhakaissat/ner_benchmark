"""Preprocessing treatment and how to format the data to sent it
    To another provider 
"""
from tqdm import tqdm
import pandas as pd
import numpy as np
import json
import random


def read_data(data_path:str):
    return pd.read_csv(data_path,sep="\t",names=["word","entity"])

def check_dataset(dataset:pd.DataFrame):
    """Check the dataset if it has nan values and other things"""
    print(dataset.isnull().sum().sum())
    print(dataset['word'].isnull().sum())
    print(dataset['entity'].isnull().sum())

def remove_nans(dataset:pd.DataFrame ):
    """Remove Nan values fom the dataset!"""
    #print(dataset.shape)
    dataset.dropna(axis=0, inplace=True)
    dataset.reset_index(drop=True, inplace=True)

    #print(dataset.shape)
    return dataset

def create_str_from_column(column_name:str, end_index:int, begin_index, dataset: pd.DataFrame):
    """Create a string of words or entities from the rows of the dataset.

    Parameters
    ----------
    column_name : str
        Name of the column you want to get the string from.
    end_index : int
        last index, is not included, see more details in `Notes`
    begin_index : int, optional
        index to begin grabbing the data, by default 0
    dataset : pd.DataSet, optional
        the dataset , by default dataset

    Returns
    -------
    str

    Notes
    -----
    We break when the `end_index==index`, so the `end_index` isn't added to 
    the output.
    TODO: test! 
    TODO: refactor because it's disgusting rn
    """
    column_str = ""

    # take care of the last elements! (this way it's included)
    if (end_index >= dataset.shape[0]):
        end_index = dataset.shape[0]
    # when we do list[begin:end] the end index is not included!
    for column_value in dataset[column_name][begin_index:end_index]:
            column_str = column_str + column_value + " "
    
    return column_str

def map_entity_token(entities_list: str, word_list: str):
    """Map each entity with it's corresponding word start and beggining.

    Parameters
    ----------
    entities_list :  str
        the list of entities
    word_list :  str
        list of words associated with the entities 
        (they are in sync)

    Returns
    -------
    entities_dic : dict
        {'entities': [
            (start, end, entity),
            (start, end, entity),
            (start, end, entity),...
            ]
        }

    TODO: Refactor this !!
    """
    start = 0
    end = 0
    chunk = []# first element the string, second the dictionnary!
    entities_dic = {}
    entities_dic["entities"] = []
    i = 0
    entities_list = entities_list.split()
    word_list = word_list.split()

    if len(entities_list) != len(word_list):
        print("not equal leng")
        exit()

    while( i < len(entities_list) ):
        tokens_count = 1
        label = entities_list[i]
        word = word_list[i]
        
        if( label == "O"):
            start = start + len(word) + 1
            #print(start)
            i += 1
            continue
        #print(start)
        label_general = label.split("-")[0] # i dont need the subentity
        
        
        # check if it's the same label after it will mean that its the same entity
        j = i
        j += 1
        try:#TODO:look up this issue, it's weird!!
            next_label = entities_list[j]
        except IndexError:
            break

        end_leng = start + len(word) + 1
        while( (label == next_label) and (j < len(entities_list)) ):
            tokens_count += 1 
            try:
                end_leng = end_leng  + len(word_list[j]) + 1 
                j += 1
                label = next_label
                next_label = entities_list[j]
            except IndexError:
                break
        
        
        if tokens_count > 1:
            i = i + tokens_count - 1
            end = end_leng - 2
        else:
            end = start + len(word) - 1
        # create the structure! 
        entities_dic["entities"].append( (start, end , label_general))
        
        
        start = end + 2
        i += 1
        #print(i,end=" ")
        
    return entities_dic

def generate_random_ent_word(dataset, chunks_length, random_list_pick :list[int]=[]):
    """generate random numbers from a list in order to create random texts 

    Parameters
    ----------
    dataset : pd.DataFrame 
        the dataset.. 
    chunks_length : 
        _description_
    radom_list_pick : list[int]
        used for optimization purposes, the idea is to delete an element
        from this list corresponding to a used index every time this function gets used
        so we will gain time and use less and less space.

    Returns
    -------
    str, str, list[int]
        string  of words, string of entities, random_list_pick
    """
    word_number = 0
    str_entities = ""
    str_words = "" 
    
    # loop
    while (word_number < chunks_length):
        random_index = []
        if random_list_pick.shape[0] == 0:
            print("all the dataset has been used!!")
            return None, None, None

        random_index.append(np.random.randint(0, random_list_pick.shape[0])) # low (inclusive) to high (exclusive).[low,hight[
        random_index_value = random_list_pick[random_index[0]] # otherwise we can have a time where a value in random_list_pick is 
            # higher then it's length so it will never get used (and i debugged and
            # find that case)   


        actual_entity = dataset["entity"][random_index_value] 

        if (actual_entity != "O"):
            # TODO: imagine if it doensnt exist (the index before or after!)
            # we do nothing!!!
            # 1. DEAL WITH THE LEFT SIDE 
            previous_index_value = random_index_value - 1
            while (previous_index_value  in random_list_pick): # check if it exists
                # check if its the same entity
                previous_entity = dataset["entity"][previous_index_value] 
                if (previous_entity == actual_entity) :
                    previous_index = np.where(random_list_pick ==previous_index_value)[0] #[0] because it returns a tuple of 1 elem
                    if len(previous_index) > 1:
                        print("ERROR there is a duplicate value in random_list_pick ")
                        exit()
                    else:
                        previous_index = previous_index[0]
                        # add it at beggining of list using insert
                        random_index.insert(0, previous_index) 
                else:# not the same so we will not use it and not look for others!
                    break
                
                previous_index_value = previous_index_value - 1

            
            # 2. DEAL WITH THE RIGHT SIDE 
            # preform same operation but for the next element
            next_index_value = random_index_value + 1
            while(next_index_value in random_list_pick):
                # check if its the same entity
                next_entity = dataset["entity"][next_index_value] 
                if (next_entity == actual_entity) :
                    # add it to the things! 
                    # increment some value?
                    next_index = np.where( random_list_pick == next_index_value)[0] #[0] because it returns a tuple of 1 elem
                    if len(next_index) > 1:
                        print("ERROR there is a duplicate value in random_list_pick ")
                        exit()
                    else:
                        next_index = next_index[0]
                        # add it at the end of list using 
                        random_index.append(next_index) 
                else:
                    break
                next_index_value = next_index_value + 1

        for i in random_index:
            #------------------------------------------------------------------------------------
            # here we have the random index
            # now we create the strings!
            index_value = random_list_pick[i]
            str_entities = str_entities + dataset["entity"][index_value] + " "
            str_words = str_words + dataset["word"][index_value] + " "
            #delete the index 
            word_number += 1
        random_list_pick = np.delete(random_list_pick, random_index) 
        
    return str_words, str_entities, random_list_pick 

def generate_random_ent_word_1tok(dataset, chunks_length, random_list_pick :list[int]=[]):
    """generate random numbers from a list in order to create random texts 
    an incomplete entity (thus the 1token)

    Parameters
    ----------
    dataset : pd.DataFrame 
        the dataset.. 
    chunks_length : 
        _description_
    radom_list_pick : list[int]
        used for optimization purposes, the idea is to delete an element
        from this list corresponding to a used index every time this function gets used
        so we will gain time and use less and less space.

    Returns
    -------
    str, str, list[int]
        string  of words, string of entities, random_list_pick
    """
    word_number = 0
    str_entities = ""
    str_words = "" 
    
    # loop
    while (word_number < chunks_length):
        random_index = []
        if random_list_pick.shape[0] == 0:
            print("all the dataset has been used!!")
            return None, None, None

        random_index = np.random.randint(0, random_list_pick.shape[0]) # low (inclusive) to high (exclusive).[low,hight[
        random_index_value = random_list_pick[random_index] # otherwise we can have a time where a value in random_list_pick is 
            # higher then it's length so it will never get used (and i debugged and
            # find that case)   



        #------------------------------------------------------------------------------------
        # here we have the random index
        # now we create the strings!
        str_entities = str_entities + dataset["entity"][random_index_value] + " "
        str_words = str_words + dataset["word"][random_index_value] + " "
        #delete the index 
        word_number += 1
        random_list_pick = np.delete(random_list_pick, random_index) 
        
    return str_words, str_entities, random_list_pick 

def generate_new_random_format_data(chunks_length :int, dataset: pd.DataFrame, max_leng :int=1000):
    """From the original dataset generate the dataset with RANDOM words with the spacy format.
    the goal is to have meaningless text in order to test it. 

    Parameters
    ----------
    chunks_length : int
        number of words for a "row"
    dataset : pd.DataFrame 
        the original dataframe generated from reading original Nerd data
    max_leng : int
        number of elements to use in the dataset!
        if all are used it will be slow thats why the default is 1000

    Returns
    -------
    list[ list [ dict{ list[tuple] } ] ]
        strucured this way: 
        [
            [ "text "
                {'entities': [
                    (start, end, entity),
                    (start, end, entity),
                    (start, end, entity),...
                ]
            }
            ],
            [ "text"
                {'entities': [
                    (start, end, entity),
                    (start, end, entity),...
                ]
            }
        ] 
    TODO: gerer le cas des dernier chunk_length valeur!! (les dernier 100 valeurs)
    """
    data_chunks = []
    random_list_pick = np.arange(dataset.shape[0])
    for last_index in tqdm(range(chunks_length, max_leng, chunks_length)):

        first_index = last_index - chunks_length
        all_input, all_entities, random_list_pick = generate_random_ent_word_1tok(dataset=dataset, chunks_length=chunks_length, random_list_pick=random_list_pick )
        if all_input==None :# if it's None it means all the dataset has been explored
            return data_chunks
        entities_result = map_entity_token(all_entities, all_input)
        data_chunk = []
        data_chunk.append(all_input)
        data_chunk.append(entities_result)
        data_chunks.append(data_chunk)
    
    #add the last ones:) (i know this code IS NOT eleguent)
    if (max_leng > last_index):
        chunks_length = max_leng - last_index
        all_input, all_entities, random_list_pick = generate_random_ent_word_1tok(dataset=dataset, chunks_length=chunks_length, random_list_pick=random_list_pick )
        entities_result = map_entity_token(all_entities, all_input)
        data_chunk = []
        data_chunk.append(all_input)
        data_chunk.append(entities_result)
        data_chunks.append(data_chunk)
    return data_chunks    

def generate_new_format_data(chunks_length :int, dataset: pd.DataFrame, max_leng :int=1000):
    """From the original dataset generate the dataset with the spacy format.

    Parameters
    ----------
    chunks_length : int
        number of words for a "row"
    dataset : pd.DataFrame 
        the original dataframe generated from reading original Nerd data
    max_leng : int
        number of elements to use in the dataset!
        if all are used it will be slow thats why the default is 1000

    Returns
    -------
    list[ list [ dict{ list[tuple] } ] ]
        strucured this way: 
        [
            [ "text "
                {'entities': [
                    (start, end, entity),
                    (start, end, entity),
                    (start, end, entity),...
                ]
            }
            ],
            [ "text"
                {'entities': [
                    (start, end, entity),
                    (start, end, entity),...
                ]
            }
        ] 
    TODO: gerer le cas des dernier chunk_length valeur!! (les dernier 100 valeurs)
    """
    data_chunks = []
    for last_index in tqdm(range(chunks_length, max_leng, chunks_length)):

        # TODO: the issue of the last 100 (maybe they are less!)
        first_index = last_index - chunks_length
        all_input = create_str_from_column("word", last_index, first_index, dataset )
        
        all_entities = create_str_from_column("entity", last_index, first_index, dataset )
        #print(i,end=" ")

        entities_result = map_entity_token(all_entities, all_input)
        
        data_chunk = []
        data_chunk.append(all_input)
        data_chunk.append(entities_result)
        data_chunks.append(data_chunk)
    
    # I KNOW ITS NOT ELEGENT 
    if max_leng  > last_index:
        first_index = last_index
        last_index = max_leng
        all_input = create_str_from_column("word", last_index, first_index, dataset )
        
        all_entities = create_str_from_column("entity", last_index, first_index, dataset )
        #print(i,end=" ")

        entities_result = map_entity_token(all_entities, all_input)
        
        data_chunk = []
        data_chunk.append(all_input)
        data_chunk.append(entities_result)
        data_chunks.append(data_chunk)
    return data_chunks

def save_data(save_path:str, file_name:str, object_to_save ):
    """
    Parameters
    ----------
    save_path : str
        should be ending with /
    file_name : str
        should have the extension 
    object_to_save 
        the python variable to be saved in the file_name
    """
    with open(save_path+file_name,"w", encoding="utf-8") as file:
        json.dump(object_to_save, file, indent=4)

def create_random_new_format(chunks_length :int, dataset: pd.DataFrame ):
    """Create a text that have no sence!(random)

    Parameters
    ----------
    chunks_length : int
        number of words for a "row"
    dataset : pd.DataFrame
        original dataset
    """
    exluded_indexes = []
    word_numbers = 0

    # 1.TODO : generate random indexes 
     
    # 2.TODO : check if the next or previous indexes are of the same entity

    # 2.1.TODO : add them to the list  (str)
    # 2.2.TODO : take  care of the case where the addition of previous and/or next indexes leads to surpass
    #           chunks_length
    pass


def run_prep_pipe(save_path, file_name, data_path):
    """Run preprocess pipeline"""
    #TODO: create data chunks
    #TODO: save data
    dataset = read_data(data_path)
    check_dataset(dataset)
    dataset = remove_nans(dataset)
    processed_data = generate_new_random_format_data(100, dataset, max_leng = dataset.shape[0])
    
    save_data(save_path, file_name, processed_data)

def read_parsed_data(data_path):
    with open(data_path) as file:
        res = json.load(file)
    return res

def fix_ibm_end():
    """function to fix ibm's issue with the fact that it defines the end of 
    an entity like this
    Skyfall
    ^      ^
    start  end  
    rather then like this
    Skyfall
    ^     ^
    start end
    """
    # TODO: loop through all ibm transformed json's
    # TODO: - 1 a tout les end!

    path_ibm_parsed = "dataset/transformed/ibm/"
            
    for i in range(150):
        file_name = f"ibm_parsed{i}.json"
        data_path = f"{path_ibm_parsed}{file_name}"
        # TODO: read the file, 
        with open(data_path) as file:
            content = json.load(file)
            #import pdb; pdb.set_trace()
            for j in range( len(content["entities"]) ):
                content["entities"][j][1] = content["entities"][j][1] - 1
        save_data(save_path=path_ibm_parsed, file_name=file_name, object_to_save = content)
        print(i) 
        entities_dic = read_data(data_path=data_path)

def generate_complete_entities():
    data_path_list = ["dataset/normal/Few-NERD(SUP)_datst/dev.txt",
    "dataset/normal/Few-NERD(SUP)_datst/test.txt",
     "dataset/normal/Few-NERD(SUP)_datst/train.txt"]
    file_name_list = ["dev_transformed_random_1tok.json",
        "test_transformed_random_1tok.json",
        "train_transformed_random_1tok.json"]
    save_path = "dataset/transformed/random_text/incomplete_entity/"
    for i in range(3):
        print(f"generate {i}... en cours\n")
        file_name = file_name_list[i]
        data_path = data_path_list[i]
        run_prep_pipe(save_path=save_path, file_name=file_name ,data_path=data_path)

if __name__ == '__main__':
    generate_complete_entities()