"""Calls to the different providers to get thier reponses"""

import os
import requests
from dotenv import load_dotenv
from preprocessing import save_data
import json
from tqdm import tqdm

def named_entity_recognition(text, language="en-US", providers=['microsoft', 'amazon', 'ibm','google']):
    """call edenai's api for ner

    Parameters
    ----------
    text : str
    language : str, optional
        by default "en-US"
    providers : list, optional
        list of providers to use, by default ['microsoft', 'amazon', 'ibm','google']

    Returns
    -------
    json
        original response from edenai!
    """
    load_dotenv()
    EDENAI_API_KEY = os.getenv('EDENAI_API_KEY')
    url = "https://api.edenai.run/v1/pretrained/text/named_entity_recognition"

    headers = {'Authorization': 'Bearer {}'.format(EDENAI_API_KEY)}

    data = {
        'language': '{}'.format(language),
        'providers': "{}".format(str(providers)),
        'text': '{}'.format(text)
    }

    response = requests.post(url, headers=headers, data=data)
    response = response.json()

    check_response(response)

    #parsed_response = parse_response(response)
    return response
 
def check_response(response):
    if 'error' in response:
        print(response['error']['type'])
        if response['error']['type'] == 'Bad Credentials':
            print("bad api key")  
            exit()

def get_provider_dic(list_importances, list_entities, list_categories):

        dic_ent = {}
        entities_list = []
        for ind, elem in enumerate(list_entities):
            tmp_entity = {}
            tmp_entity["name"] = elem
            tmp_entity["category"] = list_categories[ind]
            tmp_entity["importance"] = list_importances[ind]
            entities_list.append(tmp_entity)
        dic_ent["entities"] = entities_list
        return dic_ent

def parse_response(response):

    parsed_result = {}
    # get every provider!
    results = response["result"]
    for provider_result in results:
        # get the provider's entities returned results parsed by edenAI
        definitive_result = provider_result["result"]
        list_entities = definitive_result["entities"]
        list_importances = definitive_result["importances"]
        list_categories = definitive_result["categories"]

        entities = get_provider_dic(
            list_importances, list_entities, list_categories)
        provider = provider_result["provider"]
        parsed_result[provider] = entities

    return parsed_result

def parse_microsoft(ms_result):
    """parse microsoft original response to spacy format

    Parameters
    ----------
    amz_result : dict
        original Microsoft Azure response from the response of eden ai

    Returns
    -------
    dict 
        {'entities': [
            (start, end, entity),
            (start, end, entity),
            (start, end, entity),...
            ]
        }
    """

    # TODO:we need to check before that it's microsfort and that there is sucess status   
        # before entering this function! 
    ## map microsoft categories into "my normalized " naming convetnion
        # person ,  location organization event
        #todo think of adding date,
    entity_map = {"Location":"location",
                "Organization":"organization",
                "Person":"person",
                "Event":"event"
                }
    
    entities_dic = {}
    entities_dic["entities"] = []
    
    spacy_ms = [] #TODO: change var name
    documents_list = ms_result["original_result"]["documents"][0]
    entity_list = documents_list["entities"]
    # check for warning
    if len(documents_list["warnings"]) != 0:
        print(documents_list["warnings"] )
        print("WARNING by Microsoft!")
    for entity in entity_list:
        start = entity["offset"] 
        end = start + entity["length"] - 1
        entity_type = entity["category"].lower()
        # todo: add it to the new ms treated dataset!
        entities_dic["entities"].append((start, end, entity_type))
    
    return entities_dic
        
def parse_amazone(amz_result):
    """parse amazone original response to spacy format

    Parameters
    ----------
    amz_result : dict
        original amazone response from the response of eden ai

    Returns
    -------
    dict 
        {'entities': [
            (start, end, entity),
            (start, end, entity),
            (start, end, entity),...
            ]
        }
    """
    # key is the original value is the new!
    entity_map = {"LOCATION": "location",
        "ORGANIZATION":"organization",
        "PERSON":"person",
        "EVENT":"event"
     }

    entities_dic = {}
    entities_dic["entities"] = []

    entity_list = amz_result["original_result"]["Entities"]
    for entity in entity_list: 
        start = entity["BeginOffset"]
        # endOffset is 1 character behind the end end of the word.
        end = entity["EndOffset"] - 1
        entity_type = entity["Type"].lower()
        
        entities_dic["entities"].append((start, end, entity_type))

    return entities_dic

def parse_google(google_result):
    """parse google  original response to spacy format

    Parameters
    ----------
    amz_result : dict
        original Google Cloud response from the response of eden ai

    Returns
    -------
    dict 
        {'entities': [
            (start, end, entity),
            (start, end, entity),
            (start, end, entity),...
            ]
        }

    Warnings
    --------
        About the way i get the start of an entity
        ==========================================
        i do it through the mentions but in the case if there is
        multiple occurences of a word/entity, this solutions will
        **FAIL** because what i do is pick the first one in the 
        mentions list, and this might get a false start, the sart
        of the other occurence of that word/entity.
        So it's important to keep that in mind during the benchmark.

    Note
    ----
    For some reason google thinks that it's better to not add the key `beginOffset` when the entity 
    is in the beggining of the text!
    """
    entity_map = {
        "LOCATION": "location",
        "ORGANIZATION":"organization",
        "PERSON":"person",
        "EVENT":"event"
     }
     
    entities_dic = {}
    entities_dic["entities"] = []

    entities_list = google_result["original_result"]["entities"]
    for entity in entities_list:
        
        # find the start
        for mention in entity["mentions"]:
            if mention["text"]["content"] == entity["name"]:
                #this solution is absolutely not sure it's a "bricolage"
                try: 
                    start = mention["text"]["beginOffset"]
                except KeyError:
                    start = 0
                break
        #try : 
        end = start + len(entity["name"]) - 1
        #except Exception as e:
        #    print(f"inside exception: {e}")
            #continue
        entity_type = entity["type"].lower()
        
        entities_dic["entities"].append((start, end, entity_type))
    return entities_dic
   
def parse_ibm(ibm_result):
    """parse ibm original response to spacy format

    Parameters
    ----------
    amz_result : dict
        original ibm Watson Cloud response from the response of eden ai

    Returns
    -------
    dict 
        {'entities': [
            (start, end, entity),
            (start, end, entity),
            (start, end, entity),...
            ]
        }

    Note
    ----
    Eden ai is using the v1 of ibm so it will only detect the entities managed by
    this version, 
        About the way we deal with the inexistance of the event entity
        ==============================================================
        Since it's v1 we don't have event but we have some "equivalent" types which are:
            1. NaturalEvent 
            2. SportingEvent
        and that's what we did.

        - Another way around would be to use subentities, we will need to add a condition
        to check if the entity type is one of the 4 that we are interested in, and 
        if it's not the case we will check if the ["disambiguation"]["subtype"] exists and 
        then see inside if we have something of an event (like CricketTournamentEvent ect(there
        are a lot more just check the doc))



    Warnings
    --------
        About the way i get the start of an entity
        ==========================================
        * **same as thing i did with google**
        * i do it through the mentions but in the case if there is
        multiple occurences of a word/entity, this solutions will
        **FAIL** because what i do is pick the first one in the 
        mentions list, and this might get a false start, the sart
        of the other occurence of that word/entity.
        So it's important to keep that in mind during the benchmark.
    """

    # key is the original , value is the new
    entity_map = {"Location": "location",
        "Organization":"organization",
        "Person":"person",
        "Event":"event"
     }

    entities_dic = {}
    entities_dic["entities"] = []

    entity_list = ibm_result["original_result"]["entities"]
    for entity in entity_list: 
        # the first 0 is to just une the same mention
        try : 
            start = entity["mentions"][0]["location"][0]
            end = entity["mentions"][0]["location"][1]
        except:
            raise Exception("start or/and end cannot be found in this sample (or all), please check the json")
        entity_type = entity["type"].lower()
        # deal with the inexisance of the entity type event 
        if entity_type == "naturalevent" or entity_type == "sportingevent":
            entity_type = "event"

        entities_dic["entities"].append((start, end, entity_type))

    return entities_dic 


def main(providers):
    google_path = "dataset/transformed/google/random_text/complete_entity/"
    amazone_path = "dataset/transformed/amazone/random_text/complete_entity/"
    microsoft_path = "dataset/transformed/microsoft/random_text/complete_entity/"
    ibm_path = "dataset/transformed/ibm/random_text/complete_entity/"
    responses_path = "dataset/responses/random_text/complete_entity/microsoft/"
    # read preprocessed dataset (it's list of lists!!)
    data_path = "dataset/transformed/random_text/complete_entity/dev_transformed_random_all_tok.json"
    # TODO:loop through "each element"
    with open(data_path) as file_data:
        data_preproc = json.load(file_data)

    for i in tqdm(range (150)):
        #TODO : get the text
        text = data_preproc[i][0]
        #TODO: send the request!
        response = named_entity_recognition(text=text,providers=providers)
        #TODO: save original response (done! to be tested mais normalement makallah)
        file_name = f"reponse_{i}.json"
        save_data(save_path=responses_path, file_name=file_name, object_to_save=response)
        # TODO: parse providers and save them
        results = response["result"]

        for provider in results: # yes this ifs are BAD i know
            if provider["provider"]=="microsoft":
                tmp_list = []
                ms_parsed = parse_microsoft(provider)
                tmp_list.append(text)
                tmp_list.append(ms_parsed)
                file_name = f"microsoft_parsed{i}.json"
                save_data(save_path=microsoft_path, file_name=file_name, object_to_save=ms_parsed)

            if provider["provider"]=="amazon":
                tmp_list = []
                amz_parsed = parse_amazone(provider)
                tmp_list.append(text)
                tmp_list.append(amz_parsed)
                file_name = f"amazon_parsed{i}.json"
                save_data(save_path=amazone_path, file_name=file_name, object_to_save=amz_parsed)
            
            if provider["provider"]=="ibm":
                tmp_list = []
                ibm_parsed = parse_ibm(provider)
                tmp_list.append(text)
                tmp_list.append(ibm_parsed)
                file_name = f"ibm_parsed{i}.json"
                save_data(save_path=ibm_path, file_name=file_name, object_to_save=ibm_parsed)

            if provider["provider"]=="google":
                tmp_list = []
                ggl_parsed = parse_google(provider)
                tmp_list.append(text)
                tmp_list.append(ggl_parsed)
                file_name = f"google_parsed{i}.json"
                save_data(save_path=google_path, file_name=file_name, object_to_save=ggl_parsed)




def add_google_parsed():
    """fix the error that google had before here
    """
    google_path = "dataset/transformed/google/"
    responses_path = "dataset/responses/"

    # read preprocessed dataset (it's list of lists!!)
    for i in tqdm(range (150)):
        file_name = f"reponse_{i}.json"
        response_path = responses_path+file_name
        with open(response_path) as file_data:
            response = json.load(file_data)
         
        results = response["result"]
        text = results[0]["result"]["text"]
        for provider in results: # yes this ifs are BAD i know

            if provider["provider"]=="google":
                tmp_list = []
                ggl_parsed = parse_google(provider)
                tmp_list.append(text)
                tmp_list.append(ggl_parsed)
                file_name = f"google_parsed{i}.json"
                save_data(save_path=google_path, file_name=file_name, object_to_save=ggl_parsed)

        
if __name__ == '__main__':
    #add_google_parsed()
    providers = ['microsoft']
    main(providers=providers)


